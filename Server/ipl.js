//1st
/*const numberOfMatchesPlayedPerYear =(matches) => {
    //console.log(matches)
    return matches.reduce((a, match) => {
       // console.log(acc)                                                           //1
        if(a[match.season] ) { 
             a[match.season]++
            } 
         else {
            a[match.season] =1;
            }
            //console.log(acc)
        return a;
    },{})
    
}*/
//2nd
    /*const matchesWonPerTeamPerYear = (matches) => {
        return matches.reduce((acc,match) => {
            if (! acc[match.season]) {
                acc[match.season] = {}
            }
            if (acc[match.season][match.winner]) {
                acc[match.season][match.winner] ++                   //2nd answer method (i)
            }
            else {
                acc[match.season][match.winner] =1
            }
            return acc;
        },{})
    }*/

//3rd
const extraRunsGivenIn2016 = (matches,deliveries) => {
    var matches2016 =[ ]
      matches.filter(match =>{
        if (match.season === '2016'){                       //filtered 2016 matches
          matches2016.push(parseInt(match.id))
        }
      })
    return deliveries.reduce((extraruns, delivery) => {
        // console.log(extraruns)
      if ((delivery.match_id >= matches2016[0] && delivery.match_id <= matches2016[matches2016.length-1]) ) {
          if (extraruns[delivery.bowling_team]) {   
            extraruns[delivery.bowling_team] = extraruns[delivery.bowling_team] + parseInt(delivery.extra_runs)         //count numbers of extra runs given by bowling team
          }
          
          else {
            extraruns[delivery.bowling_team] =parseInt( delivery.extra_runs)
          }
        }
        return extraruns;
    },{})
  }

//4th
  const economyBowler = (matches,deliveries) => {
    // console.log(matches)
    var matches2015 =[ ]
      matches.filter(match =>{  
        if (match.season === '2015'){                       //filtered matches played in 2015
          matches2015.push(parseInt(match.id))
        } 
      })
      // console.log(matches2015)
    var balls = deliveries.reduce((ballsbowled, delivery) => {
        if (parseInt(delivery.match_id) >=matches2015[0] && parseInt(delivery.match_id) <= matches2015[matches2015.length-1]) { 
        if (delivery.noball_runs === "0" && delivery.wide_runs==="0"){
          if ( ballsbowled[delivery.bowler]) {
            ballsbowled[delivery.bowler]++                    //count numbers of balls bowled by particualar bowler
          }
          else {
            ballsbowled[delivery.bowler] = 1
          }
        }
        }
        // console.log(ballsbowled[delivery.bowler]);
        return ballsbowled
      }, {})
    
    
    
      var runs = deliveries.reduce((runsgiven, delivery) => {
        if (parseInt(delivery.match_id) >=matches2015[0] && parseInt(delivery.match_id) <= matches2015[matches2015.length-1]) {                                       // Function Will return total number of run given by bowler   
        if (runsgiven[delivery.bowler]) {
            runsgiven[delivery.bowler] = runsgiven[delivery.bowler] + parseInt(delivery.total_runs)         //count numbers of runs given by bowler
          }
          else {
            runsgiven[delivery.bowler] = parseInt(delivery.total_runs)
          }
        }
        // console.log(runsgiven[delivery.bowler]);
        return runsgiven
      }, {})
    var run = Object.values(runs)   
    // console.log(run)                  //runs given by bowler
    var ball = Object.values(balls)
    // console.log(ball)                   //balls bowled by bowler
    var player = Object.keys(balls)
    // console.log(player);
    var economy = [];
    var sortedEconomy = [];
    for (let i = 0; i < run.length; i++) {  
        economy.push((run[i] / ball[i]) * 6)        //calculate economy 
      }
      // console.log(economy);
      for (let j = 0; j < economy.length; j++) {
        sortedEconomy.push(economy[j])                        
      }
      // console.log(sortedEconomy)
      sortedEconomy.sort(function (a, b) {
        return a - b                              //sorting economy
      })
      //console.log(sortedEconomy)
      var result = {}
      for (var j = 0; j < 10; j++) {
        var top10 = economy.indexOf(sortedEconomy[j]) 
        result[player[top10]] = sortedEconomy[j]         
      }
      return result;}


//module.exports.numberOfMatchesPlayedPerYear = numberOfMatchesPlayedPerYear;
// module.exports.matchesWonPerTeamPerYear =matchesWonPerTeamPerYear;
//module.exports.extraRunsGivenIn2016 = extraRunsGivenIn2016;
 module.exports.economyBowler = economyBowler;
